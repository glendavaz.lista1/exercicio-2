
/**
 * Write a description of class Principal here.
 *
 * @author (Glenda Vaz)
 * @version (a version number or a date)
 */
import java.util.Scanner;

public class Principal
{
   public static void main(String[] args){
       
       Scanner le = new Scanner(System.in);       
    
       System.out.print('\u000C'); 
       Pessoa pessoa = new Pessoa();
       
       System.out.println("Insira a quantidade de anos de vida da pessoa:");
       pessoa.setAnos(le.nextInt());
       
       System.out.println("Insira a quantidade de meses:");
       pessoa.setMeses(le.nextInt());
       
       System.out.println("Insira a quantidade de dias:");
       pessoa.setDias(le.nextInt());
       
       System.out.println("Numero de dias de vida: " + pessoa.executaConversao());       
     
    
   }

 
}
