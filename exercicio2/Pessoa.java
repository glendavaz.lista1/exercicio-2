
public class Pessoa
{  
    private int anos;
    private int meses;
    private int dias;

    /**
     * Constructor for objects of class Pessoa
     */
    public Pessoa()
    {

    }
    
    int retornaIdadeEmDias() {
        return 1;
    }
    
    public void setAnos(int paramAnos){        
        anos = paramAnos;        
    }
    
    public int getAnos() {
        return anos;
    }
    
    public void setMeses(int paramMeses){
        meses = paramMeses;
    }
    
    public int getMeses() {
        return meses;
    }
    
    public void setDias(int paramDias){
        dias = paramDias;
    }
    
    public int getDias() {
        return dias;
    }
    
    public int executaConversao() {
        int anos = this.anos * 365;
        int meses = this.meses * 30;
        
        int total = anos + meses + this.dias;
        return total;
    }    
    
}
